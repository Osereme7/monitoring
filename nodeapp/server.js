const express = require('express');
const client = require('prom-client');
const app = express();
const port = 3000;

const counter = new client.Counter({
  name: 'http_requests_total',
  help: 'Total number of HTTP requests'
});

app.get('/ping', (req, res) => {
  counter.inc();
  res.json({ message: 'pong' });
});

app.get('/metrics', (req, res) => {
  res.set('Content-Type', client.register.contentType);
  res.end(client.register.metrics());
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});